package controllers;

import play.mvc.*;

import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render());
    }

    public Result guitars() {
	     return ok(guitars.render());
    }

    public Result drums() {
	     return ok(drums.render());
    }

    public Result keyboards() {
	     return ok(keyboards.render());
    }

    public Result amps() {
	     return ok(amps.render());
    }

    public Result mics() {
	     return ok(mics.render());
    }

    public Result lighting() {
	     return ok(lighting.render());
    }

    public Result events() {
	     return ok(events.render());
    }

    public Result contact() {
	     return ok(contact.render());
    }

}
